class Controllable:
    def __init__(self, builder, device, iid):
        """
        :type device: Device
        """
        self.builder = builder
        self.prefix = 'magicontrol{}'.format(iid)
        self.device = device

    def get_object(self, name):
        name = name.replace('magicontrol', self.prefix)
        return self.builder.get_object(name)


class Device:
    def __init__(self, config):
        self.config = config

    def connect(self):
        pass
