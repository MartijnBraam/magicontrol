import gi
import logging

from yaml import load

from magicontrol.bmdatem import BMDAtemDevice, BMDAtemME
from magicontrol.behringermixer import BehringerX32Device, BehringerX32Volume

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, GObject, Gio, GdkPixbuf

gi.require_version('Handy', '0.0')
from gi.repository import Handy

logging.basicConfig(level=logging.DEBUG)


class MagicontrolApplication(Gtk.Application):
    def __init__(self, application_id, flags):
        Gtk.Application.__init__(self, application_id=application_id, flags=flags)
        self.connect("activate", self.new_window)

    def new_window(self, *args):
        AppWindow(self)


class AppWindow:
    def __init__(self, application):
        self.application = application
        builder = Gtk.Builder()
        with pkg_resources.path('magicontrol', 'magicontrol.glade') as ui_file:
            builder.add_from_file(str(ui_file))
        builder.connect_signals(Handler(builder))

        window = builder.get_object("main_window")
        window.set_application(self.application)

        data = load(open('../test.yml'))
        devices = {}
        for label in data['devices']:
            device = data['devices'][label]
            if device['type'] == 'BMDAtem':
                devices[label] = BMDAtemDevice(dict(device))
            if device['type'] == 'BehringerX32':
                devices[label] = BehringerX32Device(dict(device))
        iid = 0

        main_box = builder.get_object('main_box')
        bottombar_box = builder.get_object('bottombar_box')
        sidebar_box = builder.get_object('sidebar_box')

        print("Loading main box controls")
        for c in data['layout']['main']:
            device = devices[c['device']]
            print(device)
            ctype = c['control']
            builder, instance = self.load_controllable(device, ctype, iid)
            root = builder.get_object('magicontrol{}'.format(iid))
            main_box.pack_start(root, True, True, True)
            iid += 1

        print("Loading bottombar box controls")
        for c in data['layout']['bottombar']:
            device = devices[c['device']]
            print(device)
            ctype = c['control']
            builder, instance = self.load_controllable(device, ctype, iid)
            root = builder.get_object('magicontrol{}'.format(iid))
            bottombar_box.pack_start(root, True, True, True)
            iid += 1

        window.show_all()

        Gtk.main()

    def load_controllable(self, device, ctype, iid):
        controllable_class = None
        controllable_ui = None
        if ctype == 'BMDAtemME':
            controllable_class = BMDAtemME
            controllable_ui = 'bmd_atem_me.glade'
        if ctype == 'BehringerX32Volume':
            controllable_class = BehringerX32Volume
            controllable_ui = 'behringer_x32_volume.glade'

        builder = Gtk.Builder()
        with pkg_resources.path('magicontrol', controllable_ui) as ui_file:
            with open(str(ui_file)) as handle:
                ui = handle.read()
            ui = ui.replace('"magicontrol', '"magicontrol{}'.format(iid))
            builder.add_from_string(str(ui))
        instance = controllable_class(builder, device, iid)
        builder.connect_signals(instance)
        instance.init_controls()
        return builder, instance


class Handler:
    def __init__(self, builder):
        self.builder = builder
        self.window = builder.get_object('main_window')

    def on_quit(self, *args):
        Gtk.main_quit()

    def on_start(self, *args):
        pass


def main():
    app = MagicontrolApplication("nl.brixit.magicontrol", Gio.ApplicationFlags.FLAGS_NONE)
    app.run()


if __name__ == '__main__':
    Handy.Column()
    main()
