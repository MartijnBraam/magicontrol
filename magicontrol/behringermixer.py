import queue
import threading
import time
import struct

import pythonosc.udp_client as osc
import socket

import gi

from magicontrol.controllable import Device, Controllable

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, GObject, Gio, GdkPixbuf, Gdk


class BehringerX32OSC(threading.Thread):
    def __init__(self, device):
        threading.Thread.__init__(self)
        self.device = device
        self.callback_meters = None
        self.callback_state = None
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.sendqueue = queue.Queue()

    def set_callback_meters(self, callback):
        self.callback_meters = callback

    def set_callback_state(self, callback):
        self.callback_state = callback

    def run(self):
        self.socket.connect((self.device.ip, 10024))
        self.get_info()
        self.get_meters()
        self.enable_xremote()
        timeout = time.time() + 5
        has_state = False
        while True:
            raw = self.socket.recv(4096)
            res = osc.OscMessage(raw)
            if res.address == '/info':
                print(res)
            elif res.address == '/meters/1':
                self.handle_meters_all(res.params[0])
            else:
                self.handle_state(res)

            if not self.sendqueue.empty():
                message = self.sendqueue.get()
                self.socket.send(message.dgram)

            if timeout < time.time():
                timeout = time.time() + 5
                print("TIMEOUT!")
                if not has_state:
                    self.get_initial_state()
                    has_state = True
                self.get_meters()
                self.enable_xremote()

    def get_info(self):
        message = osc.OscMessageBuilder("/info").build()
        self.socket.send(message.dgram)

    def get_initial_state(self):
        for i in range(0, 16):
            message = osc.OscMessageBuilder("/ch/{:02}/config/name".format(i)).build()
            self.socket.send(message.dgram)
            message = osc.OscMessageBuilder("/ch/{:02}/config/color".format(i)).build()
            self.socket.send(message.dgram)
            message = osc.OscMessageBuilder("/ch/{:02}/mix/fader".format(i)).build()
            self.socket.send(message.dgram)
            message = osc.OscMessageBuilder("/ch/{:02}/mix/on".format(i)).build()
            self.socket.send(message.dgram)

    def get_meters(self, channel=None):
        if channel is None:
            message = osc.OscMessageBuilder("/meters")
            message.add_arg('/meters/1')
            message = message.build()
        else:
            message = osc.OscMessageBuilder("/meters").add_arg("/meters/0").add_arg(channel).build()
        self.socket.send(message.dgram)

    def enable_xremote(self):
        message = osc.OscMessageBuilder("/xremote").build()
        self.socket.send(message.dgram)

    def handle_info(self, *args):
        pass

    def handle_meters_all(self, blob):
        if self.callback_meters is not None:
            raw = struct.unpack('<i40h', blob)
            GLib.idle_add(self.callback_meters, raw[1:])

    def handle_state(self, message):
        if self.callback_state is not None:
            GLib.idle_add(self.callback_state, message)

    def queue_message(self, message):
        self.sendqueue.put(message)


class BehringerX32Device(Device):
    def __init__(self, config):
        super().__init__(config)
        self.ip = self.config['ip']

        self.thread = BehringerX32OSC(self)
        self.thread.daemon = True
        self.thread.start()

    def connect(self):
        pass

    def send(self, commands):
        pass

    def on_data(self, *args):
        pass

    def queue_osc(self, messages):
        self.thread.queue_message(messages)


class BehringerX32Volume(Controllable):
    def init_controls(self):
        self.device.thread.set_callback_meters(self.callback_meters)
        self.device.thread.set_callback_state(self.callback_state)
        volumebox = self.get_object('magicontrol_volumebox')

        channels = []
        for i in range(1, 17):
            channels.append(str(i))

        screen = Gdk.Screen.get_default()
        provider = Gtk.CssProvider()
        style_context = Gtk.StyleContext()
        style_context.add_provider_for_screen(screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        css = b"""
        button.muted {
            /* background: red; */
            color: red;
        }
        label.scribble.color0 {
            background: black;
            color: #ccc;
        }
        label.scribble.color1 {
            background: red;
            color: black;
        }
        label.scribble.color2 {
            background: #0A0;
            color: black;
        }
        label.scribble.color3 {
            background: #DD0;
            color: black;
        }
        label.scribble.color4 {
            background: #44F;
            color: black;
        }
        label.scribble.color5 {
            background: #F0F;
            color: black;
        }
        progressbar.meter progress{
            background: green;
        }
        scale.slider highlight {
            background: none;
        }
        scale.slider slider {
            
        }
        """
        provider.load_from_data(css)
        self.meters = []
        self.faders = []
        self.scribblestrips = []
        self.mutes = []

        for cidx, channel in enumerate(channels):
            channelstrip = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
            channelstrip.set_size_request(48, 0)

            scribblestrip = Gtk.Label()
            scribblestrip.set_text(channel)
            scribblestrip.get_style_context().add_class('scribble')
            scribblestrip.get_style_context().add_class('color0')
            channelstrip.pack_start(scribblestrip, False, True, False)

            solo = Gtk.Button.new_with_label("Solo")
            channelstrip.pack_start(solo, False, True, False)

            meterbox = Gtk.Box()
            level = Gtk.Adjustment()
            level.set_upper(1.0)
            level.set_lower(0.0)
            level.set_step_increment(1.0 / 1024)
            slider = Gtk.Scale.new(Gtk.Orientation.VERTICAL, level)
            slider.set_inverted(True)
            slider.set_draw_value(False)
            slider.get_style_context().add_class('slider')
            slider.add_mark(0, Gtk.PositionType.LEFT, "∞")
            slider.add_mark(0.7, Gtk.PositionType.LEFT, "0")
            slider.add_mark(1, Gtk.PositionType.LEFT, "+10")
            meterbox.pack_start(slider, True, True, True)
            meter = Gtk.ProgressBar()
            meter.set_inverted(True)
            meter.get_style_context().add_class('meter')
            self.meters.append(meter)
            self.faders.append(level)
            self.scribblestrips.append(scribblestrip)
            meter.set_orientation(Gtk.Orientation.VERTICAL)
            meterbox.pack_end(meter, False, True, True)
            channelstrip.pack_start(meterbox, True, True, True)

            mute = Gtk.Button.new_with_label("Mute")
            mute.channel = cidx + 1
            mute.muted = False
            mute.connect("clicked", self.on_mute_clicked)
            self.mutes.append(mute)
            channelstrip.pack_start(mute, False, True, False)

            volumebox.pack_start(channelstrip, True, True, False)

    def callback_meters(self, raw):
        max = 22000  # I don't know either
        for i, meter in enumerate(self.meters):
            meter.set_fraction((max + raw[i]) / float(max))

    def callback_state(self, raw):
        address = raw.address
        if address.startswith('/ch/'):
            channel = int(address[4:6])
            param = address[6:]
            print("Updating fader {} with {}".format(channel, param))
            if param == '/mix/fader':
                self.faders[channel - 1].set_value(raw.params[0])
            elif param == '/config/name':
                self.scribblestrips[channel - 1].set_text(raw.params[0])
            elif param == '/config/color':
                strip = self.scribblestrips[channel - 1]
                newcolor = raw.params[0]
                for col in range(0, 16):
                    strip.get_style_context().remove_class('color{}'.format(col))
                strip.get_style_context().add_class('color{}'.format(newcolor))
                print("Setting color {}".format(newcolor))
            elif param == '/mix/on':
                if raw.params[0] == 1:
                    self.mutes[channel - 1].muted = True
                    self.mutes[channel - 1].get_style_context().remove_class('muted')
                else:
                    self.mutes[channel - 1].muted = False
                    self.mutes[channel - 1].get_style_context().add_class('muted')

    def on_mute_clicked(self, button):
        channel = button.channel
        message = osc.OscMessageBuilder("/ch/{:02}/mix/on".format(channel))
        if button.muted:
            message.add_arg(0)
            button.muted = False
            self.mutes[channel - 1].get_style_context().remove_class('muted')
        else:
            message.add_arg(1)
            button.muted = True
            self.mutes[channel - 1].get_style_context().add_class('muted')
        self.device.queue_osc(message.build())
