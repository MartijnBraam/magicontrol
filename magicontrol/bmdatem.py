import socket
import gi

from magicontrol.controllable import Device, Controllable

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, GObject, Gio, GdkPixbuf, Gdk


class BMDAtemDevice(Device):
    SYNC = 1
    CONNECT = 2
    REPEAT = 4
    UNKNOWN = 8
    ACK = 16

    def __init__(self, config):
        super().__init__(config)
        self.ip = self.config['ip']
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.model = 'TVS'

    def connect(self):
        pass

    def send(self, commands):
        pass


class BMDAtemME(Controllable):
    def init_controls(self):
        program = self.get_object('magicontrol_grid_program')
        preview = self.get_object('magicontrol_grid_preview')

        if self.device.model == 'TVS':
            buttons = [
                ['CAM1', 'CAM2', 'CAM3', 'CAM4', 'CAM5', 'BLK', 'COL2'],
                ['CAM6', 'CAM7', 'CAM8', '    ', '    ', 'Bars', 'MP1', 'MP2']
            ]

        program.set_hexpand(True)
        preview.set_hexpand(True)

        screen = Gdk.Screen.get_default()
        provider = Gtk.CssProvider()
        style_context = Gtk.StyleContext()
        style_context.add_provider_for_screen(screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        css = b"""
        button.bmdatem {
            padding: 0;
            margin: 0;
        }
        button.bmdatem label {
            font-size: 9pt;
            font-weight: bold;
        }
        button.bmdatem.preview.active {
            background: #4F4;
            color: black;
        }
        button.bmdatem.program.active {
            background: #F44;
            color: black;
        }
        """
        provider.load_from_data(css)

        for top, row in enumerate(buttons):
            for left, button in enumerate(row):
                active = button.strip() != ""

                label = Gtk.Label(label=button)

                btn = Gtk.Button()
                btn.add(label)
                btn.set_sensitive(active)
                btn.set_size_request(48, 48)
                btn.get_style_context().add_class('bmdatem')
                btn.get_style_context().add_class('program')
                if top + left == 0:
                    btn.get_style_context().add_class('active')
                program.attach(btn, left, top, 1, 1)

                plabel = Gtk.Label(label=button)
                pbtn = Gtk.Button()
                pbtn.add(plabel)
                pbtn.set_sensitive(active)
                pbtn.set_size_request(48, 48)
                pbtn.get_style_context().add_class('bmdatem')
                pbtn.get_style_context().add_class('preview')
                if top + left == 0:
                    pbtn.get_style_context().add_class('active')
                preview.attach(pbtn, left, top, 1, 1)
