from setuptools import setup

setup(
    name='magicontrol',
    version='0.1.0',
    packages=['magicontrol'],
    url='https://gitlab.com/MartijnBraam/magicontrol',
    license='MIT',
    author='Martijn Braam',
    author_email='martijn@brixit.nl',
    description='Universal AV production control panel',
    long_description=open("README.rst").read(),
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7',
        'Operating System :: POSIX :: Linux',
    ],
    install_requires=[
        'pyyaml>=5.1.2',
    ],
    zip_safe=True,
    include_package_data=True,
    entry_points={
        'gui_scripts': [
            'magicontrol=magicontrol.__main__:main'
        ]
    }
)
