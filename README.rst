Magicontrol
===========

This is a python GTK3 application that can control video production hardware.
The basic UI is inspired by the Blackmagic Design ATEM control software.

Features
--------

This program isn't in an usable state yet. The basic pluggable UI loading works and there's an UI element for ATEM control.

Screenshots
-----------

.. image:: data/screenshot-1.png
